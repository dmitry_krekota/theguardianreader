angular.module('theGuardianReader')
    .controller('theGuardianReader.topController',
        ['$scope', '$routeParams', 'theGuardianService',
            function($scope, $routeParams, theGuardianService) {

                theGuardianService.getSections(function(sections) {
                    $scope.sections = sections;
                    if (!$scope.currentSection && $routeParams.sectionId) {
                        $scope.setCurrentSection($routeParams.sectionId);
                    }
                });

                $scope.setCurrentSection = function(sectionId) {
                    var i;

                    for (i = 0; i < $scope.sections.length; i++) {
                        if ($scope.sections[i].id == sectionId) {
                            $scope.currentSection = $scope.sections[i];
                            return;
                        }
                    }
                };

            }
        ]);