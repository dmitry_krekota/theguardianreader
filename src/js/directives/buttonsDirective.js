angular.module('theGuardianReader')
    .directive('customButtons', [
        function() {
            return {
                restrict: 'E',
                scope: true,
                templateUrl: 'src/views/directives/buttons.html',
                link: function($scope, $element, $attrs) {
                    $scope.tempArray = [];
                    $scope.tempArray[$attrs.count-1] = undefined;
                }
            };
        }
    ]);
