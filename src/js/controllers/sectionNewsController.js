angular.module('theGuardianReader')
    .controller('theGuardianReader.sectionNewsController',
        ['$scope', '$http', '$routeParams', 'theGuardianService',
            function($scope, $http, $routeParams, theGuardianService) {

                theGuardianService.getNews($routeParams.sectionId, function(news) {
                    $scope.news = news;
                });

            }
        ]);