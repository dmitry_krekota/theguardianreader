angular.module('theGuardianReader', ['ngRoute'])
    .config(['$routeProvider', 'theGuardianServiceProvider',
        function($routeProvider, theGuardianServiceProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'src/views/home.html',
                    controller: 'theGuardianReader.homeController'
                })
                .when('/news/:sectionId', {
                    templateUrl: 'src/views/section_news.html',
                    controller: 'theGuardianReader.sectionNewsController'
                })
                .otherwise({
                    redirectTo: '/'
                });
            theGuardianServiceProvider.setApiKey('0b9df86c-94f5-495a-a9c4-db1e9cfcc047');
        }]);