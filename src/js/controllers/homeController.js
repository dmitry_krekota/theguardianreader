angular.module('theGuardianReader')
    .controller('theGuardianReader.homeController',
        ['$scope', '$location',
            function($scope, $location) {
                $scope.$parent.currentSection = null;

                $scope.languages = [
                    {value: 'Arabic', key: 'ar'},
                    {value: 'Catalan', key: 'ca'},
                    {value: 'Chinese, simplified', key: 'zh_CN'},
                    {value: 'Chinese, traditional', key: 'zh_TW'},
                    {value: 'Czech', key: 'cs'},
                    {value: 'Danish', key: 'da'},
                    {value: 'Dutch', key: 'nl'},
                    {value: 'English', key: 'en'},
                    {value: 'Finnish', key: 'fi'},
                    {value: 'French', key: 'fr'},
                    {value: 'German', key: 'de'},
                    {value: 'Greek', key: 'gr'},
                    {value: 'Hungarian', key: 'hu'},
                    {value: 'Indonesian', key: 'id'},
                    {value: 'Italian', key: 'it'},
                    {value: 'Japanese', key: 'ja'},
                    {value: 'Korean', key: 'ko'},
                    {value: 'Lithuanian', key: 'lt'},
                    {value: 'Malay', key: 'ms'},
                    {value: 'Norwegian', key: 'no'},
                    {value: 'Polish', key: 'pl'},
                    {value: 'Portuguese', key: 'pt'},
                    {value: 'Russian', key: 'ru'},
                    {value: 'Spanish', key: 'es'},
                    {value: 'Swedish', key: 'sv'},
                    {value: 'Turkish', key: 'tr'},
                    {value: 'Ukrainian', key: 'uk'},
                    {value: 'Vietnamese', key: 'vi'}
                ];

                $scope.myLanguage = 'uk';

                $scope.formData = {
                    section: null
                };

                $scope.goToSection = function(section) {
                    $scope.setCurrentSection(section.id);
                    $location.path('/news/' + section.id);
                };
            }
        ]);