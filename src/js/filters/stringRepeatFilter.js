angular.module('theGuardianReader')
    .filter('stringRepeat', [function() {
        return function(value, count) {
            if(angular.isUndefined(count)){
                count = 2;
            }
            return value.repeat(count)

        };
    }]);