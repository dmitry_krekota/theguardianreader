angular.module('theGuardianReader')
    .component('helloTheGuardian', {
        template: '<p>{{ helloText }}, {{ $ctrl.theGuardianText }} !</p>',
        controller: ['$scope', function($scope) {
            this.theGuardianText = 'TheGuardian';
            $scope.helloText = 'Hello';
        }]
    });
